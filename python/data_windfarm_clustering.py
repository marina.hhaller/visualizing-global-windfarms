# import numpy as np
import pandas as pd
from helpfile import *
# import json
# from sklearn import metrics

# from collections import Counter
pd.options.mode.chained_assignment = None  # default='warn'

# Read data
# data_full = pd.read_csv("data/data_world_processed.csv")#.loc[:,-1]
# data_full = data_full.drop('Unnamed: 0', axis = 1)

data_clean = pd.read_csv("data/data_processed.csv")#.loc[:,-1]
if 'Unnamed: 0' in data_clean.columns:
    data_clean = data_clean.drop('Unnamed: 0', axis = 1)
# data_raw = data_raw.drop('Unnamed: 0', axis = 1)


# get results from different sizes for epsilon 
for el in [0.8, 0.9, 1, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2, 3]:
    res  = dbscan_func(el, 4, data_clean, storefile=False)
    clustered_data,df_grouped = res
    data_clean["WFid_" + str(el)] = clustered_data["WFid"]
    #Computing "the Silhouette Score"
    # print(str(el)+ " - Silhouette Coefficient: %0.3f"
    #     % metrics.silhouette_score(clustered_data[["moll_x", "moll_y"]], clustered_data["WFid"]))
data_clean.to_csv("data/data_processed_allclusters.csv")


# Create final clustered set 
data = pd.read_csv("data/data_processed.csv")#.loc[:,-1]
data_offshore = data[data["Land Cover"] == "Oceans and seas"]
data_onshore = data[data["Land Cover"] != "Oceans and seas"]

res_onshore, t  = dbscan_func(1.5, 2, data_onshore, storefile=False)
res_offshore, t = dbscan_func(2.2, 2, data_offshore, storefile=False)

data_offshore.loc[:,"WFid"] = (res_offshore["WFid"]+100001).astype("int32")
data_onshore.loc[:,"WFid"] = res_onshore["WFid"].astype("int32")
frames = [data_offshore, data_onshore]
data_clustered = pd.concat(frames)
data_clustered.loc[data_clustered["WFid"] == 100000, ["WFid"]] = -1
data_clustered.iloc[:,1::].to_csv("data/data_clustered.csv")
#Computing "the Silhouette Score"

