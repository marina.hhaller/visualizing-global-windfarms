# from re import X
import pandas as pd
import statistics as stats


def create_data_agg_by_wfid(): 
    wfid_column = "WFid"
    data = pd.read_csv("data/data_clustered.csv")#.iloc[1:5000]
    data[["Turbine Spacing",  "Elevation"]] = data[["Turbine Spacing",  "Elevation"]].round()
    
    # count nr turbines in park
    wfid_count = data[wfid_column].value_counts()
    wfid_count = wfid_count.to_frame()
    wfid_count.columns = ["Number of turbines"] #change column name

    # take modus value of non numerical columns 
    data_categorical_agg_by_wfid = data[["Country","Continent", "Land Cover", "Landform",wfid_column]].groupby([wfid_column]).agg(lambda x:  stats.mode(x))

    # take mean of numerical columns 
    data_numerical_agg_by_wfid = data[["lon", "lat", "Turbine Spacing",  "Elevation",wfid_column]].groupby([wfid_column]).mean()
    # Merge sets
    windFarms_aggregated_dataset = pd.merge(data_numerical_agg_by_wfid, data_categorical_agg_by_wfid,left_index=True, right_index=True) # merge two datasets 
    windFarms_aggregated_dataset = pd.merge(windFarms_aggregated_dataset, wfid_count,left_index=True, right_index=True) # merge two datasets 

    labels = pd.read_csv("data/data_enrichment/WFshapes_pred.csv")[["WFid", "Shape"]]
    labels = labels.set_index("WFid")

    # Add shape 
    windFarms_aggregated_dataset = windFarms_aggregated_dataset.join(labels)
    windFarms_aggregated_dataset.loc[-1, "Number of turbines"] = 1
    windFarms_aggregated_dataset.loc[windFarms_aggregated_dataset["Shape"].isnull(), ["Shape"]] = "Less than 5 turbines"
    windFarms_aggregated_dataset.loc[-1, "Shape"] = "Single turbine"

    # shape_list = np.insert(labels, 0, -10) 
    # len(shape_list)
    # windFarms_aggregated_dataset["Shape"] = shape_list


    #  Roudn numeric values and save in memory saving type 
    windFarms_aggregated_dataset[["Number of turbines"]] = windFarms_aggregated_dataset[["Number of turbines"]].astype("int16")
    windFarms_aggregated_dataset.loc[~windFarms_aggregated_dataset["Elevation"].isnull(), ["Elevation"]] = windFarms_aggregated_dataset.loc[~windFarms_aggregated_dataset["Elevation"].isnull(), ["Elevation"]].astype("int16")
    windFarms_aggregated_dataset.loc[~windFarms_aggregated_dataset["Turbine Spacing"].isnull(), ["Turbine Spacing"]] = windFarms_aggregated_dataset.loc[~windFarms_aggregated_dataset["Turbine Spacing"].isnull(), ["Turbine Spacing"]].astype("int16")

    # Reset column names 
    windFarms_aggregated_dataset = windFarms_aggregated_dataset.reset_index()
    windFarms_aggregated_dataset.columns =  [['WFid', 'lon', 'lat', 'Turbine Spacing', 'Elevation', 'Country', 'Continent', 'Land Cover', 'Landform', 'Number of turbines', 'Shape']] 
    # Write data
    # windFarms_aggregated_dataset.to_csv("data/aggregate_wf_data.csv" ) #write WF data
    windFarms_aggregated_dataset.to_csv("data/wf_data_final.csv" ) #write WF data


    # Expand WT data with Number of turbines and Shape 
    turbinescount_df =  pd.DataFrame(windFarms_aggregated_dataset[["WFid", "Number of turbines"]])
    turbinescount_df.columns = ["WFid", "Number of turbines"]
    turbinescount_df.columns  =  turbinescount_df.columns.map(''.join) # get rid of MultiIndex
    wind_turbines_advanced_dataset = data.merge(turbinescount_df, on = "WFid", how = "left")

    # wind_turbines_advanced_dataset = data.merge(wfid_count, on = "WFid",  how ="left")
    shape_df = pd.DataFrame(windFarms_aggregated_dataset[["WFid", "Shape"]])
    turbinescount_df.columns = ["WFid", "Shape"]
    shape_df.columns  =  shape_df.columns.map(''.join) # get rid of MultiIndex
    wind_turbines_advanced_dataset = wind_turbines_advanced_dataset.merge(shape_df, on = "WFid", how = "left")
    windFarms_aggregated_dataset.to_csv("data/wt_data_final.csv" ) #write WF data

    # Write data
    return windFarms_aggregated_dataset, wind_turbines_advanced_dataset
    
create_data_agg_by_wfid()
